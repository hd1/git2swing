package us.d8u.git2swing;

import java.io.File;
import java.util.Map;
import java.util.Set;
import java.util.Vector;
import javax.swing.table.DefaultTableModel;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.lib.AnyObjectId;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.revwalk.RevCommit;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GitLogTableModel extends DefaultTableModel {
  private static final Logger log = LoggerFactory.getLogger("us.d8u.git2swing.GitLogTableModel");
  private static Git git;
	public GitLogTableModel() {
		super();
    try {

      git = Git.open(new File(System.getProperty(UI.REPOSITORY_LOCATION_KEY)));
    } catch (Exception e) { 
      log.error(e.getMessage(), e);
      System.exit(-1);
    }
    
    Map<AnyObjectId, Set<Ref>> allsRefs = git.getRepository().getAllRefsByPeeledObjectId(); 
    Iterable<RevCommit> commits= null;
    try {
      commits = git.log().call(); 
    } catch (org.eclipse.jgit.api.errors.GitAPIException e) {
      log.error(e.getMessage(), e);
    }
    for (RevCommit commit : commits) { 
      log.info("commit msg={}", commit.getShortMessage()); 
      this.addRev(commit);
    }
	}

	public void addRev(RevCommit element) {
    log.info("Adding a commit");
    if (this.getDataVector() == null) {
		  String[] identifiers = new String[] { "Revision", "Author", "Commit Message", "Date" };
      Vector<String> columnIdentifiers = new Vector<String>();
      for (String identifier : identifiers) { columnIdentifiers.add(identifier); }
      Vector<Vector<String>> data = new Vector<Vector<String>>();
      this.setDataVector(data, columnIdentifiers);
    }
		Vector<String> newRow = new Vector<String>();
		newRow.addElement(element.getName().trim());
		newRow.addElement(element.getCommitterIdent().getName().trim());
		newRow.addElement(element.getFullMessage().trim());
		newRow.addElement(AsString(element.getCommitTime()).trim());
		this.getDataVector().add(newRow);
    log.info("added "+newRow);
	}

	private static String AsString(int epochTime) {
		DateTime asDT = new DateTime(new Long(epochTime).longValue());
		DateTimeFormatter fmt = ISODateTimeFormat.dateTime();
		String ret = fmt.print(asDT);
		return ret;
	}
}
