package us.d8u.git2swing;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.net.MalformedURLException;
import java.net.URL;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.NoHeadException;
import org.eclipse.jgit.internal.storage.file.FileRepository;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.lib.StoredConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UI implements ActionListener {
  private static Logger log = LoggerFactory.getLogger(us.d8u.git2swing.UI.class);
  public static final String REPOSITORY_LOCATION_KEY = "us.d8u.git2swing.UI.repositoryLocation";
  public static final String REMOTE_URL_KEY = "us.d8u.git2swing.remoteRepositoryLocation";
  static final JTable tbl = new JTable();
  static JFrame frame = new JFrame();

  public UI() { 
    log.info("table made!");

    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    JScrollPane pane = new JScrollPane(tbl);
    log.info("scroller constructed!");

    JButton refreshButton = new JButton("Go");
    refreshButton.addActionListener(this);
    log.info("refreshButton!");

    JTextField repositoryLocationField = new JTextField();
    repositoryLocationField.addActionListener(this);
    log.info("Text Field added!");
    JPanel panel = new JPanel();
    panel.setLayout(new java.awt.BorderLayout());
    panel.add(repositoryLocationField, java.awt.BorderLayout.WEST);
    panel.add(refreshButton, java.awt.BorderLayout.EAST);
    frame.add(panel, java.awt.BorderLayout.NORTH);
    frame.add(pane, java.awt.BorderLayout.CENTER);
    frame.pack();
    log.info("packed!");
    repositoryLocationField.setSize(repositoryLocationField.getHeight(), (int)frame.getBounds().getWidth());
    pane.setSize((int)frame.getBounds().getHeight()-(int)repositoryLocationField.getHeight(), (int)frame.getBounds().getWidth());
    log.info("Resized!");
    frame.setVisible(true);
  }

  private static final Integer TEMP_DIR_ATTEMPTS = 1000;
  public void actionPerformed(ActionEvent evt) {
    String objectType = evt.getSource().getClass().getName().substring(evt.getSource().getClass().getName().lastIndexOf("."));
    log.debug("object is of type "+objectType);
    try {
      String textFieldContents = null;
      if (evt.getSource().getClass().getName().endsWith("JTextField")) {
        textFieldContents = ((JTextField)evt.getSource()).getText();
        try {
          new URL(textFieldContents);
        } catch (MalformedURLException e) {
          log.error(e.getMessage(), e);
          textFieldContents = new File("./.git").toURI().toString();
        }
        log.info("Default git repository path is "+textFieldContents);
        System.setProperty(REMOTE_URL_KEY, textFieldContents);
      }
      File tempDir = null;
      try {
        tempDir = File.createTempFile("checkout", ".git");
        tempDir.delete();
        tempDir.mkdir();
      } catch (Throwable t) {
        error(t.getMessage(), t);
      }
      System.setProperty(REPOSITORY_LOCATION_KEY, tempDir.getAbsolutePath());
      log.info("Local Checkout location: "+System.getProperty(REPOSITORY_LOCATION_KEY));
      //Git git = new Git(new FileRepository(System.getProperty(REPOSITORY_LOCATION_KEY)));
      Git git = Git.cloneRepository()
        .setCloneSubmodules(true)
        .setURI(System.getProperty(REMOTE_URL_KEY))
        .setDirectory(new File(REPOSITORY_LOCATION_KEY))
        .call();
      //new Git(path);
      // add to git config
      System.setProperty("user.dir", tempDir.getAbsolutePath()+"/.git");
      log.info(System.getProperty("user.dir") + " is our current directory");
      StoredConfig config = git.getRepository().getConfig();
      config.setString("remote", "origin", "url", System.getProperty(REMOTE_URL_KEY));
      config.save();
      git.pull().call();

      log.info(textFieldContents + " checked out to "+System.getProperty(REPOSITORY_LOCATION_KEY) + " for the duration of this run");

      // refresh model
      GitLogTableModel tableModel = new GitLogTableModel();
      tbl.setModel(tableModel);
    } catch (Exception e) {
      error(e.getMessage(), e);
    }
  }
  public static void main (String[] args) {
    UI u = new UI();
  }

  public void error(String msg, Throwable t) {
    Object errorMessage = msg;
    while (t != null) {
      log.error(msg,t);
      errorMessage = errorMessage + "\n" + t.getMessage() + t.getStackTrace();
      t = t.getCause();
    }
    JOptionPane.showMessageDialog(SwingUtilities.getRoot(frame), errorMessage, "Error", JOptionPane.ERROR_MESSAGE);
  }
}


